<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddMedicionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		return [
			'pecho'				=>'required|numeric|min:0|max:99.99',
			'brazo'				=>'required|numeric|min:0|max:99.99',
			'ante_brazo'		=>'required|numeric|min:0|max:99.99',
			'cintura'			=>'required|numeric|min:0|max:99.99',
			'cadera'			=>'required|numeric|min:0|max:99.99',
			'muslo'				=>'required|numeric|min:0|max:99.99',
			'pantorrilla1'		=>'required|numeric|min:0|max:99.99',
			'abdomen'			=>'required|numeric|min:0|max:99.99',
			'supraespinal'		=>'required|numeric|min:0|max:99.99',
			'subescapular'		=>'required|numeric|min:0|max:99.99',
			'triceps'			=>'required|numeric|min:0|max:99.99',
			'muslo_anterior'	=>'required|numeric|min:0|max:99.99',
			'pantorrilla2'		=>'required|numeric|min:0|max:99.99',
			'peso'				=>'required|numeric|min:0|max:99.99',
			'grasa'				=>'required|numeric|min:0|max:99.99',
			'musculo'			=>'required|numeric|min:0|max:99.99',
			'objetivo'			=>'required|min:5|max:255',
			'alimentacion'		=>'required|min:5|max:255',
			'consideraciones'	=>'required|min:5|max:255',

		];
	}

}

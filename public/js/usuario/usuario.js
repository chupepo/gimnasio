$(document).ready(function () {

   /*Buscador de la tabla de usuarion en home */
    $(function () {
    
        $( '#table' ).searchable({
            striped: true,
            oddRow: { 'background-color': '#f5f5f5' },
            evenRow: { 'background-color': '#fff' },
            searchType: 'fuzzy'
            });
              
            $( '#searchable-container' ).searchable({
              searchField: '#container-search',
              selector: '.row',
              childSelector: '.col-xs-4',
              show: function( elem ) {
                  elem.slideDown(100);
              },
              hide: function( elem ) {
                  elem.slideUp( 100 );
              }
        });
    });

    $("#divCita").click(function (e) {
        e.preventDefault();
        $('.divTableUsers').hide(700);
        $('.cita').show(700);
    });
    $("#divVerCitas").click(function (e) {
        e.preventDefault();
        $('.divTableUsers').hide(700);
        $('.cita').hide(700);
    });
    

});

    /*
    |--------------------------------------------------------------------------------
    | Boton cancela la orden de un formulario y redirecciona a la pagina anterior
    |--------------------------------------------------------------------------------
    */
    function cancel(){
      window.location.href = "/home";
    }
    function cerrarCitaContent(){
        $('.divTableUsers').show(700);
        $('.cita').hide(700);
    }

@extends('app')

@section('content')
	
	<link href="{{ asset('/css/reportes/pagos.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/reportes/reportes.css') }}" rel="stylesheet">
	
	<script type="text/javascript" src="{{ asset('/js/reportes/pagos.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/reportes/reportes.js') }}"></script>
	<div class="content-all-reportes">
		
		<div class="top-list-reportes">
			@include('reportes.pagos.partials.top-list-reportes')
		</div>
		<div class="top-list-reportes">
			@include('reportes.pagos.partials.contenido-general4')
		</div>
	</div>

@endsection

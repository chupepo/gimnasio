<!DOCTYPE html>
<html>
<head>
	<title></title>
	 <!-- ... -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('/css/admin_user/css/bootstrap-datetimepicker.min.css') }}" />

  <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.min.js') }}   "></script>
  <script type="text/javascript" src="{{ asset('/js/usuario/editar/js/moment.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('/js/usuario/editar/js/bootstrap-datetimepicker.min.js') }}"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="{{ asset('/js/usuario/editar/js/java.js') }}"></script>


</head>
<body>
<form>
  <div class="container">
      <div class='col-md-5'>
          <div class="form-group">
              <div class='input-group date' id='datetimepicker6'>
                  <input type='text' class="form-control" />
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
          </div>
      </div>
      <div class='col-md-5'>
          <div class="form-group">
              <div class='input-group date' id='datetimepicker7'>
                  <input type='text' class="form-control" />
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
          </div>
      </div>
  </div>
</form>

</body>
</html>
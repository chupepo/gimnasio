
      <div class="col-md-13 col-md-offset-0">
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-4 col-lg-offset-4">
                  <input type="search" id="search" value="" class="form-control" placeholder="Buscar Usuario">
                </div>
              </div>
              <br/>
              <table class="table table-striped" id="table">
                <thead>
                  <tr>
                    <th>Avatar</th>
                    <th>Nombre</th>
                    <th>Primer</th>
                    <th>Correo</th>
                    <th>Cédula</th>
                    <th>Editar</th>
                    <th>Perfil</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($users as $user): ?>
                    <tr data-id="{{$user->id}}">
                      <td><img src="{{ asset($user->imagen)}}"style="width:50px;height:50px;"></td>
                      <td>{{$user->nombre}}</td>
                      <td>{{$user->apellido1}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->cedula}}</td>
                      <td><a href="{{route('admin.users.edit',$user->id)}}" class="btn btn-primary">Editar</a></td>
                      <td><a href="/perfil/{{$user->id}}" class="btn btn-info">Perfil</a></td>
                      <td><a href="#" class="btn btn-info">Perfil</a></td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>

              <?php echo $users->render(); ?>
            </div>
            {!! Form::open(array('url' => ['prueba',':USER_ID'], 'role'=>'form','id' => 'form-edit','method' => 'POST')) !!}
            {!! Form::close() !!}
      </div>